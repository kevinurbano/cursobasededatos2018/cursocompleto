﻿
DROP DATABASE IF EXISTS empresa;
CREATE DATABASE IF NOT EXISTS empresa;
USE empresa;

CREATE TABLE IF NOT EXISTS personas(
  id int AUTO_INCREMENT,
  nombre varchar(10),
  apellido varchar(10),
  poblacion varchar(10),
  fecha date,
  dato1 int,
  dato2 int,
  dato3 int,
  empresa int,
  fechaTrabaja date,
  PRIMARY KEY(id)
  );

CREATE TABLE IF NOT EXISTS empresas(
  codigo int AUTO_INCREMENT,
  nombre varchar(10),
  poblacion varchar(10),
  cp varchar(5),
  trabajadores int,
  PRIMARY KEY(codigo)
  );

-- Utilizando ALTER TABLE para crear las foreing keys
ALTER TABLE personas ADD CONSTRAINT FKPersonasEmpresas FOREIGN KEY (empresa)
  REFERENCES empresas(codigo) ON DELETE CASCADE ON UPDATE CASCADE;

SELECT * FROM empresas;
SELECT * FROM personas;