﻿
UPDATE personas SET dato1=0;

UPDATE personas SET dato2=0, dato3=0;

SELECT * FROM personas;



-- 1.- colocar en dato1 a los que su nombre comience por c
-- Consulta de seleccion
SELECT * FROM personas WHERE LEFT(nombre,1)='c'; 

-- Consulta de actualización
UPDATE personas SET dato1=1 WHERE LEFT(nombre,1)='c';

-- 2.- En dato2 me vais a colocar el dia del mes de hoy en dato1 si hay 1

SELECT DAYOFMONTH(NOW()) FROM personas WHERE dato1=1;
 
UPDATE personas SET dato2=DAYOFMONTH(NOW()) WHERE dato1=1;

SELECT * FROM personas WHERE dato1=1;

-- 3.- dato3 = dato1 + dato2
-- consulta de seleccion
SELECT * FROM personas WHERE QUARTER(fecha)=1;
UPDATE personas SET dato3=dato1+dato2 WHERE QUARTER(fecha)=1;
SELECT * FROM personas;

-- 4.- cambiar el valor de numero de trabajadores a 0 -- el campo trabajadores me indique cuanto trabajadores tiene esa empresa
  SELECT empresa,COUNT(*) n FROM personas GROUP BY empresa;

  UPDATE empresas SET trabajadores=0;

  SELECT empresa,COUNT(*) n FROM empresas JOIN personas ON empresas.codigo = personas.empresa GROUP BY empresa;

  UPDATE empresas 
    JOIN 
      (SELECT empresa,COUNT(*) n FROM empresas JOIN personas ON empresas.codigo = personas.empresa GROUP BY empresa) c1
    ON c1.empresa=empresas.codigo
    SET trabajadores=n;
  SELECT * FROM empresas;
