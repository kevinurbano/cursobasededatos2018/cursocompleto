﻿/* Realizamso la consulta en 1 paso*/
SELECT
  equipo.*
FROM equipo
  JOIN ciclista
    ON equipo.nomequipo = ciclista.nomequipo
  JOIN etapa
    ON ciclista.dorsal = etapa.dorsal
  JOIN puerto
    ON etapa.numetapa = puerto.numetapa;

-- Vamos a realizar por partes


-- C1 - Sacar las etapas que tienen puerto

SELECT DISTINCT  etapa.numetapa,etapa.dorsal
  FROM etapa
  INNER JOIN puerto ON etapa.numetapa = puerto.numetapa;


-- C2 - Ciclistas que hayan ganado C1 (etapas con puerto)
SELECT DISTINCT nomequipo 
  FROM ciclista c 
  JOIN (
    SELECT DISTINCT etapa.numetapa,etapa.dorsal 
    FROM etapa
    JOIN puerto ON etapa.numetapa = puerto.numetapa
    ) c1
  ON c.dorsal=c1.dorsal;

-- C3 Consulta final
  SELECT 
    * 
    FROM equipo 
    JOIN (
      SELECT DISTINCT nomequipo 
        FROM ciclista c 
        JOIN (
          SELECT DISTINCT etapa.numetapa,etapa.dorsal 
          FROM etapa
          JOIN puerto ON etapa.numetapa = puerto.numetapa
        ) c1
        ON c.dorsal=c1.dorsal
    ) c2
    ON c2.nomequipo=equipo.nomequipo;

/* VISTAS */

-- C1
CREATE OR REPLACE VIEW consulta1C1 AS
  SELECT DISTINCT  e.numetapa,e.dorsal
    FROM etapa e
    INNER JOIN puerto p ON e.numetapa = p.numetapa;

SELECT * FROM consulta1C1 c;
DROP VIEW consulta1C1;

-- C2
CREATE OR REPLACE VIEW consulta1C2 AS
  SELECT DISTINCT nomequipo 
  FROM ciclista c 
  JOIN consulta1C1 c1
  ON c.dorsal=c1.dorsal;


SELECT * FROM consulta1C2 c;
DROP VIEW consulta1C2;


-- Consulta1 
CREATE OR REPLACE VIEW consulta1 AS
  SELECT DISTINCT e.* 
  FROM equipo e
  JOIN consulta1C2 c2
  ON c2.nomequipo=e.nomequipo;

SELECT * FROM consulta1 c1;
DROP VIEW consulta1;

-- Ciclistas que hayan ganado puertos de mas de 1200 
SELECT DISTINCT c.* 
FROM ciclista c 
JOIN puerto p ON c.dorsal = p.dorsal
WHERE p.altura>1200;

-- Mejorar la consulta mediante las subconsultas

-- C1
SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura>1200;

-- Consulta completa
SELECT c.* 
FROM (SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura>1200) AS c1
JOIN ciclista c ON c.dorsal=c1.dorsal;

-- vamos a utilizar vistar 

CREATE OR REPLACE VIEW consulta2C1 AS
  SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura>1200;

CREATE OR REPLACE VIEW consulta2 AS
  SELECT c.* 
  FROM consulta2C1 AS c1p
  JOIN ciclista c ON c.dorsal=c1.dorsal;

SELECT * FROM consulta2 c;

-- Consulta 3 - Ciclistas que NO han ganado Etapas
SELECT * 
FROM ciclista c 
LEFT JOIN  etapa e ON c.dorsal=e.dorsal
WHERE e.dorsal IS NULL;

-- Realizar con sub consultas
SELECT DISTINCT e.dorsal FROM etapa e;

-- Consulta completa 
-- Resta con LEFT JOIN
SELECT c.* 
FROM ciclista c 
LEFT JOIN (SELECT DISTINCT e.dorsal FROM etapa e) AS c1 
ON c.dorsal=c1.dorsal WHERE c1.dorsal IS NULL;


-- La resta pero con NOT IN
SELECT * FROM  ciclista c WHERE c.dorsal NOT IN (SELECT DISTINCT e.dorsal FROM etapa e);


-- Realizar con Vistas
-- C1
CREATE OR REPLACE VIEW consulta3C1 AS
  SELECT DISTINCT e.dorsal FROM etapa e;

-- Consulta completa
CREATE OR REPLACE VIEW consulta3 AS
  SELECT c.* 
    FROM ciclista c 
    LEFT JOIN consulta3C1 AS c1 
    ON c.dorsal=c1.dorsal WHERE c1.dorsal IS NULL;;

CREATE OR REPLACE VIEW consulta3P AS
  SELECT * FROM  ciclista c WHERE c.dorsal NOT IN (SELECT DISTINCT e.dorsal FROM etapa e);

/* Consulta 4  */
  /* Cicliscas que no hayan ganado etapas y que no han ganado ningun puerto*/

/* C1 - Ciclistas que han ganado etapas */

SELECT DISTINCT e.dorsal FROM etapa e;

-- Vista 
CREATE OR REPLACE VIEW consulta4C1 AS
  SELECT DISTINCT e.dorsal FROM etapa e;


/* C2 - Ciclista  que han ganado puerto */
SELECT DISTINCT p.dorsal FROM puerto p;

CREATE OR REPLACE VIEW consulta4C2 AS
  SELECT DISTINCT p.dorsal FROM puerto p;

/* C3 - Ciclistas que no han ganado puerto */
SELECT c.dorsal FROM ciclista c LEFT JOIN puerto p ON c.dorsal = p.dorsal  WHERE p.dorsal IS NULL;

CREATE OR REPLACE VIEW consulta4C3 AS
  SELECT c.dorsal FROM ciclista c LEFT JOIN puerto p ON c.dorsal = p.dorsal  WHERE p.dorsal IS NULL;

/* Variaciones */

/* C1 - C3 */
/*  Ciclistas que han ganado y que han ganado puerto */

SELECT c1.dorsal FROM consulta4C1 c1 LEFT JOIN consulta4C3 c3 USING(dorsal)
  WHERE c3.dorsal IS NULL;

SELECT c1.dorsal FROM consulta4C1 c1
  WHERE c1.dorsal NOT IN (SELECT * FROM consulta4C3);

/* C1 - C2 */
/* Ciclistas que han ganado y que no han ganado puerto */

SELECT c1.dorsal FROM consulta4C1 c1 LEFT JOIN consulta4C2 c2 USING(dorsal)
  WHERE c2.dorsal IS NULL;

SELECT * FROM consulta4C1 c1 WHERE c1.dorsal NOT IN (SELECT * FROM consulta4C2);


/* C1 UNION C2 */
/* Ciclistas que han ganado etapas mas los ciclistas que han ganado puerto */

SELECT * FROM consulta4C1
UNION
SELECT * FROM consulta4C2;

/* C1 UNION C3 */
/* Ciclistas que han ganado etapas mas los ciclistas que no han ganado puerto */
SELECT * FROM consulta4C1
UNION
SELECT * FROM consulta4C3;

/* C1 intersecion C3 */
/* Ciclista que han ganado etapas y ademas que no han ganado puerto */
SELECT * FROM consulta4C1 c1 NATURAL JOIN consulta4C3 c3;

/* C1 interseccion C2 */
/* Ciclista que han ganado etapa y ademas que han ganado puerto */
SELECT * FROM consulta4C1 c1 NATURAL JOIN consulta4C2 c2;

/* Consulta 5*/

-- C1
/* Ciclistas que han llevado maillot*/
SELECT DISTINCT l.dorsal FROM lleva l;

CREATE OR REPLACE VIEW consulta5C1 AS
  SELECT DISTINCT l.dorsal FROM lleva l;

-- C2
/* Ciclistas que NO han llevado maillot*/
/* Ciclistas - C1 */
SELECT c.dorsal FROM ciclista c LEFT JOIN  consulta5C1 c1 USING(dorsal) WHERE c1.dorsal IS NULL;

SELECT c.dorsal FROM ciclista c WHERE c.dorsal NOT IN (SELECT * FROM consulta5C1);

CREATE OR REPLACE VIEW consulta5C2 AS
SELECT c.dorsal FROM ciclista c LEFT JOIN  consulta5C1 c1 USING(dorsal) WHERE c1.dorsal IS NULL;

-- C3
/* Ciclistas que han ganado etapa */
SELECT DISTINCT e.dorsal FROM etapa e;

CREATE OR REPLACE VIEW consulta5C3 AS
  SELECT DISTINCT e.dorsal FROM etapa e;
  
-- C4
/* Ciclistas que han ganado puerto */
SELECT DISTINCT p.dorsal FROM puerto p;

CREATE OR REPLACE VIEW consulta5C4 AS
  SELECT DISTINCT p.dorsal FROM puerto p;

/* Variaciones */
/* C3 - C1 */
/*Ciclistas que han ganado etapa y que NO han llevado maillot */
SELECT c3.dorsal FROM consulta5C3 c3 LEFT JOIN consulta5C1 c1 USING(dorsal) WHERE c1.dorsal IS NULL;

SELECT c3.dorsal FROM consulta5C3 c3 WHERE c.dorsal NOT IN (SELECT * FROM consulta5C1);

/* C3 INTERSECT C2 */
/*Ciclistas que han ganado etapa y que NO han llevado algun maillot */
SELECT c3.dorsal FROM consulta5C3 c3 NATURAL JOIN consulta5C2 c2;

/*En logica seria que (c3 - c1) = (c3 INTERSECT c2)

/* C3 - C2 */
SELECT c3.dorsal FROM consulta5C3 c3 LEFT JOIN consulta5C2 c2 USING(dorsal) WHERE c2.dorsal IS NULL;

SELECT c3.dorsal FROM consulta5C3 c3 WHERE c3.dorsal NOT IN (SELECT * FROM consulta5C2);

/* C3 INTERSECT C1 */
/* Ciclistas que han llevado maillot mas los que han ganado alguna etapa */
SELECT c3.dorsal FROM consulta5C3 c3 NATURAL JOIN consulta5C1 c1;

/* En logica seria que las consultas (c3 - c2) = (c3 INTERSECT c1) */

/* C4 - C3 - C2 */
/* Ciclistas que han ganado puerto y que no han ganado etapa y que han llevado maillot */
SELECT c4.dorsal 
  FROM consulta5C4 c4 
  LEFT JOIN consulta5C3 c3 USING(dorsal) 
  LEFT JOIN consulta5C2 c2 USING(dorsal) 
  WHERE c3.dorsal IS NULL AND c2.dorsal IS NULL;

/* C1 UNION C3 */
/*Ciclistas que han llevado maillot mas los que han ganado alguna etapa */
SELECT * FROM consulta5C1 c1
UNION
SELECT * FROM consulta5C3 c3;

/* C1 UNION C3 UNION C4 */
/*Ciclistas que han llevado maillot mas los que han ganado alguna etapa mas que han ganado puerto */
SELECT * FROM consulta5C1 c1
UNION
SELECT * FROM consulta5C3 c3
UNION
SELECT * FROM consulta5C4 c4;




