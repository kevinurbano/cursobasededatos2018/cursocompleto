﻿/* Consultas de Combinación Interna - 4 */

/* 1 - Nombre y edad de los ciclistas que han ganado etapas */
SELECT DISTINCT nombre,edad from ciclista c INNER JOIN etapa e ON c.dorsal=e.dorsal;

-- Mejor solución con subconsulta por que haces relacion solamente con los datos que quieres

-- C1
SELECT DISTINCT dorsal FROM etapa;

-- Consulta completa con JOIN
SELECT c.nombre,c.edad FROM (SELECT DISTINCT dorsal FROM etapa) c1 JOIN ciclista c ON c.dorsal=c1.dorsal;

-- Consulta completa con IN
SELECT nombre,edad FROM ciclista c WHERE c.dorsal IN (SELECT DISTINCT dorsal FROM etapa);

-- Creando Vista 
CREATE OR REPLACE VIEW c1 AS
  SELECT DISTINCT dorsal FROM etapa;

SELECT 
    c.nombre,c.edad 
  FROM ciclista c 
  JOIN c1 
  USING(dorsal);

DROP VIEW c1; -- Eliminamos la vista despues de hacer la consulta

/* 2 - Nombre y edad de los ciclistas que han ganado puertos */
SELECT DISTINCT nombre,edad from puerto p INNER JOIN ciclista c ON c.dorsal=p.dorsal;

-- C1
SELECT DISTINCT p.dorsal FROM puerto p;

-- Consulta completa con JOIN
SELECT 
  c.nombre,c.edad 
  FROM ciclista c 
  JOIN 
    (SELECT DISTINCT p.dorsal FROM puerto p) AS c1 
  ON c.dorsal=c1.dorsal;

-- Utilizando Vistas
CREATE OR REPLACE VIEW c1 AS
  SELECT DISTINCT p.dorsal FROM puerto p;

SELECT 
  c.nombre,c.edad 
  FROM ciclista c 
  JOIN c1 
  ON c.dorsal=c1.dorsal;

DROP VIEW c1;
 
/* 3 - Nombre y edad de los ciclistas que han ganado etapas y puertos */
SELECT DISTINCT nombre,edad FROM ciclista c 
  INNER JOIN etapa e ON c.dorsal=e.dorsal
  INNER JOIN puerto p ON c.dorsal=p.dorsal;

-- Consulta Optimizada

SELECT c.nombre,c.edad
FROM (
    SELECT * 
      FROM 
        (SELECT DISTINCT e.dorsal FROM etapa e) c1
      JOIN
        (SELECT DISTINCT p.dorsal FROM puerto p) c2
      USING(dorsal)
) c3
JOIN ciclista c 
USING(dorsal);

-- Vistas 
-- C1
/* Ganadores de etapa */
CREATE OR REPLACE VIEW c1 AS
  SELECT DISTINCT e.dorsal FROM etapa e;

-- C2
/* Ganadores de puerto */
CREATE OR REPLACE VIEW c2 AS
  SELECT DISTINCT p.dorsal FROM puerto p;

-- C3
/* Ganadores de puerto y Etapas (INTERSECT) */
CREATE OR REPLACE VIEW c3 AS
  SELECT DISTINCT * FROM c1 NATURAL JOIN c2;

-- C3 JOIN Ciclista 
SELECT c.nombre,c.edad FROM c3 JOIN ciclista c USING(dorsal);

DROP VIEW c1;
DROP VIEW c2;
DROP VIEW c3;

/* 4 - Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa */
SELECT DISTINCT director FROM equipo eq 
  INNER JOIN ciclista c ON eq.nomequipo=c.nomequipo
  INNER JOIN etapa et ON c.dorsal=et.dorsal;

-- Consulta Optimizada

SELECT director 
  FROM equipo
  JOIN 
    (
      SELECT DISTINCT c.nomequipo 
        FROM ciclista c 
        JOIN (SELECT DISTINCT dorsal FROM etapa) c1
        ON c1.dorsal=c.dorsal
    ) c2
  USING(nomequipo);

SELECT director 
  FROM equipo
  WHERE nomequipo IN (SELECT DISTINCT c.nomequipo 
                        FROM ciclista c 
                        JOIN 
                          (SELECT DISTINCT dorsal FROM etapa) c1
                        ON c1.dorsal=c.dorsal);
          
-- Vistas 
        
-- C1 - Dorsal de los ciclistas que han ganado etapas  
  CREATE OR REPLACE VIEW c1 AS
  SELECT DISTINCT dorsal FROM etapa; 

-- C2 - Nombre del equipo de los ciclistas que han ganado etapa 
  CREATE OR REPLACE VIEW c2 AS 
    SELECT DISTINCT c.nomequipo FROM ciclista c JOIN c1 USING(dorsal);

-- equipo JOIN C2
SELECT director FROM equipo e JOIN c2 USING (nomequipo);

DROP VIEW c1;
DROP VIEW c2;

/* 5 - Dorsal y nombre de los ciclistas que hayan llevado algún maillot */
SELECT DISTINCT c.dorsal,nombre FROM ciclista c INNER JOIN lleva l ON  c.dorsal=l.dorsal;

-- C1
SELECT DISTINCT l.dorsal FROM lleva l;

-- Vista 
CREATE OR REPLACE VIEW c1 AS
  SELECT DISTINCT l.dorsal FROM lleva l;

SELECT c.dorsal,c.nombre FROM ciclista c JOIN c1 USING(dorsal);

DROP VIEW c1;

/* 6 - Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo */
-- Consulta completa sin optimizar
SELECT DISTINCT 
    c.dorsal,nombre 
  FROM 
    ciclista c 
  INNER JOIN 
    lleva l ON  c.dorsal=l.dorsal 
  INNER JOIN
    maillot m ON l.código=m.código
  WHERE 
    color='Amarillo';

-- Optimizar Consultas con Vistas
-- c1 - Codigo del maillot amarillo
  CREATE OR REPLACE VIEW c1 AS
    SELECT m.código FROM maillot m WHERE m.color="Amarillo";

-- c2 - Dorsal de los ciclistas que han llevado el maillot amarillo
CREATE OR REPLACE VIEW C2 AS
SELECT 
  DISTINCT l.dorsal 
  FROM lleva l 
  WHERE l.código IN (SELECT * FROM c1);

CREATE OR REPLACE VIEW C2P AS
SELECT DISTINCT l.dorsal FROM lleva l JOIN c1 USING(código);

-- Final 
SELECT c.dorsal,c.nombre FROM ciclista c JOIN C2 USING(dorsal);

DROP VIEW C1;
DROP VIEW C2;
DROP VIEW C2P;

/* 7 - Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas */
SELECT DISTINCT l.dorsal FROM lleva l INNER JOIN etapa e ON l.numetapa=e.numetapa;

-- Optimizando consulta con Vistas

-- C1 dorsal de ciclistas que han llevado maillot
CREATE OR REPLACE VIEW c1 AS
  SELECT DISTINCT l.dorsal FROM lleva l;

-- C2 dorsal de ciclistas que han ganado etapa
CREATE OR REPLACE VIEW c2 AS
  SELECT DISTINCT e.dorsal FROM etapa e;

DROP VIEW c1;
DROP VIEW c2;

-- Final Resultado 
SELECT * FROM c1 NATURAL JOIN c2;

/* 8 - Indicar el numetapa de las etapas que tengan puertos  */
SELECT DISTINCT p.numetapa FROM puerto p;

/* 9 - Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos */
SELECT DISTINCT kms FROM etapa e 
  INNER JOIN ciclista c ON e.dorsal=c.dorsal 
  INNER JOIN puerto p ON e.numetapa=p.numetapa
  WHERE c.nomequipo='Banesto';

-- Vistas
-- C1 - Etapas que tienen puerto
CREATE OR REPLACE VIEW c1 AS
  SELECT DISTINCT p.numetapa FROM puerto p;

-- C2 - Dorsales de los ciclistas del equipo Banesto
CREATE OR REPLACE VIEW c2 AS
  SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto';

-- C3 Etapas que han ganado los ciclistas de Banesto
CREATE OR REPLACE VIEW c3 AS
SELECT numetapa FROM etapa JOIN c2 USING(dorsal);

-- C4 Etapas que tienen puerto y que han ganado ciclistas de Banesto
CREATE OR REPLACE VIEW c4 AS
SELECT * FROM c1 NATURAL JOIN c3;

-- Resultado
SELECT e.kms FROM c4 JOIN etapa e USING(numetapa);

/* 10 - Listar el número de ciclistas que hayan ganado alguna etapa con puerto */

SELECT  DISTINCT c.dorsal FROM ciclista c 
  INNER JOIN etapa e ON c.dorsal=e.dorsal
  INNER JOIN puerto p ON p.numetapa=e.numetapa; 

SELECT COUNT(*) FROM (SELECT  DISTINCT c.dorsal FROM ciclista c 
  INNER JOIN etapa e ON c.dorsal=e.dorsal
  INNER JOIN puerto p ON p.numetapa=e.numetapa) C1;

-- ---------------------------
CREATE OR REPLACE VIEW c1 AS
SELECT DISTINCT etapa.dorsal  FROM puerto p JOIN etapa USING(numetapa);

SELECT COUNT(*) AS nCiclistas FROM c1;

SELECT COUNT(DISTINCT e.dorsal) AS nciclistas FROM puerto p JOIN etapa e USING(numetapa);

/* 11 - Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto */
SELECT DISTINCT nompuerto FROM puerto p 
  INNER JOIN ciclista c ON p.dorsal=c.dorsal
  WHERE nomequipo='Banesto';

-- C1 dorsales de ciclista de Banesto
CREATE OR REPLACE VIEW c1 AS
SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto';

-- Final resultado
SELECT p.nompuerto FROM puerto p JOIN c1 USING(dorsal);

/* 12 - Listar el número de etapas que tengan puerto que hayan sido ganado por ciclistas de Banesto con mas 200 km4 */

SELECT DISTINCT e.numetapa 
  FROM etapa e 
    INNER JOIN puerto p ON e.numetapa=p.numetapa 
    INNER JOIN ciclista c ON e.dorsal=c.dorsal 
    WHERE nomequipo='Banesto' AND e.kms>200;

SELECT COUNT(*) FROM 
    (SELECT DISTINCT e.numetapa FROM etapa e 
    INNER JOIN puerto p ON e.numetapa=p.numetapa 
    INNER JOIN ciclista c ON e.dorsal=c.dorsal 
    WHERE nomequipo='Banesto' AND e.kms>200) c1;

-- Optimizando los ejercicios
-- C1 dorsales de ciclista de Banesto
CREATE OR REPLACE VIEW c1 AS
SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto';

SELECT COUNT(DISTINCT numetapa) FROM c1
  JOIN etapa e USING(dorsal)
  JOIN puerto p USING(numetapa)
  WHERE e.kms>200; 
