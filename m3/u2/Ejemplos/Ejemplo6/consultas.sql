﻿/* Consultas de Selección - 6 */

/* 1 - Numero de vendedores cuya fecha de alta sea en febrero de cualquier año. */
SELECT COUNT(*) FROM vendedores WHERE MONTH(FechaAlta)=2;

/* 2 - Numero de vendedores guapos y feos. */
SELECT `Guap@`,COUNT(*) FROM vendedores GROUP BY `Guap@`;

/* 3 - El nombre del producto más caro */
SELECT MAX(p.Precio) FROM productos p;

SELECT p.NomProducto FROM productos p WHERE p.Precio=(SELECT MAX(p.Precio) FROM productos p);

/* 4 -El precio medio de los productos por grupo */
SELECT p.IdGrupo,AVG(p.Precio) FROM productos p GROUP BY p.IdGrupo;

/* 5 -Indica que grupo tiene producto que se hayan vendido alguna vez */
SELECT DISTINCT p.IdGrupo FROM productos p JOIN ventas v ON p.IdProducto = v.`Cod Producto`;

-- Optimizado minimo
SELECT DISTINCT v.`Cod Producto` FROM ventas v;

SELECT DISTINCT p.IdGrupo FROM productos p JOIN (SELECT DISTINCT v.`Cod Producto` FROM ventas v) c1 ON p.IdProducto=c1.`Cod Producto`;

/* 6 - Indica cuales son los grupos de los cuales no se han vendido ningún producto */
-- Id del grupo que hayan realizado ventas 
SELECT DISTINCT p.IdGrupo FROM productos p JOIN ventas v ON p.IdProducto = v.`Cod Producto`;

-- Id de todos los grupos
SELECT g.IdGrupo FROM grupos g;

-- Grupos que no hayan realizado ventas 
SELECT c2.IdGrupo 
FROM 
  (SELECT DISTINCT p.IdGrupo FROM productos p JOIN ventas v ON p.IdProducto = v.`Cod Producto`) c1
LEFT JOIN
  (SELECT g.IdGrupo FROM grupos g) c2
USING (IdGrupo)
WHERE c1.IdGrupo IS NULL;

/* 7 - Número de poblaciones cuyos vendedores son guapos */
SELECT COUNT(DISTINCT v.Poblacion)TotalVendedores FROM vendedores v WHERE v.`Guap@`=TRUE;

/* 8 - Nombre de la población que tiene mas vendedores casados */

SELECT v.Poblacion,COUNT(*) totalPoblacion FROM vendedores v WHERE v.EstalCivil='Casado' GROUP BY v.Poblacion;

SELECT MAX(totalPoblacion) FROM (SELECT v.Poblacion,COUNT(*) totalPoblacion FROM vendedores v WHERE v.EstalCivil='Casado' GROUP BY v.Poblacion) c1;

SELECT 
  * 
FROM 
(SELECT v.Poblacion,COUNT(*) totalPoblacion 
  FROM vendedores v 
  WHERE v.EstalCivil='Casado' 
  GROUP BY v.Poblacion) c1
WHERE c1.totalPoblacion=
  (SELECT MAX(totalPoblacion) 
    FROM 
      (SELECT v.Poblacion,COUNT(*) totalPoblacion 
        FROM vendedores v 
        WHERE v.EstalCivil='Casado' 
        GROUP BY v.Poblacion) c1) ;

/* 9 - Población que ninguno de sus vendedores están casados */
-- C1 - Población que no hay casados
SELECT DISTINCT v.Poblacion FROM vendedores v WHERE NOT v.EstalCivil='Casado';
-- C2 - Población que hay casados
SELECT DISTINCT v.Poblacion FROM vendedores v WHERE v.EstalCivil='Casado';

-- c1 - c2
SELECT c1.Poblacion 
  FROM
    (SELECT DISTINCT v.Poblacion FROM vendedores v WHERE NOT v.EstalCivil='Casado') c1 
  LEFT JOIN 
    (SELECT DISTINCT v.Poblacion FROM vendedores v WHERE v.EstalCivil='Casado') c2
  USING(poblacion)
  WHERE c2.Poblacion IS NULL;

/* 10 - Vendedor que no ha vendido nada */

SELECT DISTINCT v.`Cod Vendedor` FROM ventas v;

SELECT v.IdVendedor FROM vendedores v LEFT JOIN ventas v1 ON v.IdVendedor = v1.`Cod Vendedor` WHERE v1.`Cod Vendedor` IS NULL;

SELECT c1.`Cod Vendedor`
  FROM 
    (SELECT DISTINCT v.`Cod Vendedor` FROM ventas v) c1 
  LEFT JOIN
    (SELECT v.IdVendedor FROM vendedores v LEFT JOIN ventas v1 ON v.IdVendedor = v1.`Cod Vendedor` WHERE v1.`Cod Vendedor` IS NULL) c2
  ON c1.`Cod Vendedor`=c2.IdVendedor
  WHERE c1.`Cod Vendedor` IS NULL;

/* 11 - El vendedor que ha vendido mas Kilos. Quiero conocer su nombre */

-- C1 vendedor y la suma de sus kilos vendidos     
SELECT v.`Cod Vendedor`,SUM(v.Kilos) totalKilos FROM ventas v GROUP BY v.`Cod Vendedor`;

-- C2 maximo total de los kilos vendidos
SELECT MAX(c1.totalKilos) FROM (SELECT v.`Cod Vendedor`,SUM(v.Kilos) totalKilos FROM ventas v GROUP BY v.`Cod Vendedor`) c1;

-- C3 Codigo de vendedor con el maximo total de kilos vendidos
SELECT 
  c1.`Cod Vendedor`
FROM 
  (SELECT v.`Cod Vendedor`,SUM(v.Kilos) totalKilos FROM ventas v GROUP BY v.`Cod Vendedor`) c1
WHERE c1.totalKilos=
  (SELECT MAX(c1.totalKilos) FROM (SELECT v.`Cod Vendedor`,SUM(v.Kilos) totalKilos FROM ventas v GROUP BY v.`Cod Vendedor`) c1);

-- Nombre del vendedores con el maximo total de kilos vendidos
SELECT v.NombreVendedor
  FROM 
    vendedores v 
  JOIN 
    (SELECT c1.`Cod Vendedor`FROM (SELECT v.`Cod Vendedor`,SUM(v.Kilos) totalKilos FROM ventas v GROUP BY v.`Cod Vendedor`) c1
      WHERE c1.totalKilos=
    (SELECT MAX(c1.totalKilos) FROM (SELECT v.`Cod Vendedor`,SUM(v.Kilos) totalKilos FROM ventas v GROUP BY v.`Cod Vendedor`) c1))c1
  ON v.IdVendedor=c1.`Cod Vendedor`;