﻿DROP DATABASE IF EXISTS consultaAccion1;
CREATE DATABASE IF NOT EXISTS consultaAccion1;
USE consultaAccion1;

CREATE TABLE IF NOT EXISTS productos(
  id int AUTO_INCREMENT,
  nombre varchar(15),
  precio float,
  peso int,
  grupo varchar(15),
  cantidad int,
  PRIMARY KEY(id)
  )ENGINE INNODB;

CREATE TABLE IF NOT EXISTS cliente(
  id int AUTO_INCREMENT,
  nombre varchar(15),
  apellidos varchar(15),
  descuento float,
  cantidad int,
  nombreCompleto varchar(50),
  PRIMARY KEY(id)
  )ENGINE INNODB;

CREATE TABLE IF NOT EXISTS venden(
  producto int,
  cliente int,
  cantidad int,
  fecha date,
  total int,
  PRIMARY KEY(producto,cliente)
  )ENGINE INNODB;

ALTER TABLE venden 
  -- Primera clave ajena
  ADD CONSTRAINT FKVendenProducto 
  FOREIGN KEY (producto) 
  REFERENCES productos(id) ON DELETE CASCADE ON UPDATE CASCADE,
  -- Segunda clave ajena
  ADD CONSTRAINT FKVendenCliente 
  FOREIGN KEY (cliente) 
  REFERENCES cliente(id) ON DELETE CASCADE ON UPDATE CASCADE;
