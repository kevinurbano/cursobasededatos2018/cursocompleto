﻿

-- Establecer valor NULL en campos derviados 

-- Productos
SELECT * FROM productos;
UPDATE productos SET cantidad=NULL;

-- Cliente
SELECT * FROM cliente;
UPDATE cliente SET cantidad=NULL;
UPDATE cliente SET nombreCompleto=NULL;

-- Venden
SELECT * FROM venden;
UPDATE venden set total=NULL;

-- Venden.total = producto.precio*venden.cantidad
SELECT precio*venden.cantidad FROM venden JOIN productos ON venden.producto = productos.id;
UPDATE venden JOIN productos ON venden.producto = productos.id SET total=precio*venden.cantidad;

-- NombreCompleto = nombre + apellido
SELECT CONCAT_WS(' ',nombre,apellidos) FROM cliente;
UPDATE cliente SET nombreCompleto=CONCAT_WS(' ',nombre,apellidos);

SELECT precio*venden.cantidad*(1-descuento) FROM venden 
  JOIN productos ON venden.producto = productos.id
  JOIN cliente ON venden.cliente = cliente.id;

-- 5A Venden.total = producto.precio*venden.cantidad*(1-cliente.descuento)
SELECT p.precio, v.cantidad, c.descuento,
  precio*v.cantidad*(1-descuento/100)    
  FROM 
    productos p 
  JOIN 
    venden v ON p.id = v.producto
  JOIN 
    cliente c ON c.id =v.cliente;

UPDATE venden 
  JOIN productos ON venden.producto=productos.id
  JOIN cliente ON venden.cliente = cliente.id 
  SET total=precio*venden.cantidad*(1-descuento/100);

-- 5B Productos.cantidad = Suma de todos los productos vendidos 

CREATE OR REPLACE VIEW productosVendidos AS
SELECT producto,SUM(cantidad) suma FROM venden GROUP BY producto;

UPDATE 
  (SELECT producto,SUM(cantidad) suma FROM venden GROUP BY producto) c1 
  JOIN productos ON c1.producto=productos.id
  SET productos.cantidad=suma;

UPDATE 
  productosVendidos c1 
  JOIN productos ON c1.producto=productos.id
  SET productos.cantidad=suma;

-- 5C Cliente.cantidad = Suma de todos los productos comprados por ese cliente
CREATE OR REPLACE VIEW productosPorCliente AS
SELECT cliente,SUM(cantidad) suma FROM venden GROUP BY cliente;

UPDATE
  (SELECT cliente,SUM(cantidad) suma FROM venden GROUP BY cliente) c1
  JOIN cliente ON c1.cliente=cliente.id
  SET cliente.cantidad=c1.suma;

UPDATE
  productosPorCliente c1
  JOIN cliente ON c1.cliente=cliente.id
  SET cliente.cantidad=c1.suma;

-- 6A Añadir un campo en la tabla productos denominado precioTotal de tipo Float
ALTER TABLE productos ADD COLUMN precioTotal float;

-- 6B Añadir un campo en la tabla cliente denominado precioTotal de tipo Float
ALTER TABLE cliente ADD COLUMN precioTotal float;

-- 7A Productos.PrecioTotal = Suma de todos los precios de ese productos vendidos
CREATE OR REPLACE VIEW totalProducto AS
SELECT producto,SUM(total) suma FROM venden GROUP BY producto;

UPDATE 
  (SELECT producto,SUM(total) suma FROM venden GROUP BY producto) c1
  JOIN productos ON c1.producto=productos.id
  SET precioTotal=c1.suma;

UPDATE 
  totalProducto c1
  JOIN productos ON c1.producto=productos.id
  SET precioTotal=c1.suma;

-- 7B Cliente.PrecioTotal = Suma de todos los precios del cliente

CREATE OR REPLACE VIEW TotalCliente AS
SELECT cliente,SUM(total) suma FROM venden GROUP BY cliente;

UPDATE 
  (SELECT cliente,SUM(total) suma FROM venden GROUP BY cliente) c1
  JOIN cliente  ON c1.cliente=cliente.id
  SET precioTotal=c1.suma;

UPDATE 
  TotalCliente c1
  JOIN cliente  ON c1.cliente=cliente.id
  SET precioTotal=c1.suma;

SELECT * FROM cliente;
SELECT * FROM venden;
SELECT * FROM productos;



