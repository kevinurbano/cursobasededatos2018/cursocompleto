﻿/* Consultas de Seleccion y Totales - 3 */

/* 1 - Listar las edades de todos los ciclistas de Banesto */
SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Banesto';

/* 2 - Listar las edades de los ciclistas que son de Banesto o de Navigare */
SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Banesto' OR nomequipo='Navigare';

SELECT DISTINCT edad FROM ciclista WHERE nomequipo IN ('Banesto','Navigare'); -- Mejor esto

/* 3 - Listar el dorsal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32 */
SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 25 AND 32;

/* 4 - Listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32 */
SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32;

/* 5 - Listar la inicial del equipo de los ciclistas cuyo nombre comience por R */
SELECT LEFT(nomequipo,1) FROM ciclista WHERE nombre LIKE 'R%'; -- el like va lento 

-- sustituir el LIKE  por una funcion mas rapida
-- aunque no de problemas las mayusculas vamos a evitarlos
-- Convertimos los datos en mayusculas con la funcion upper() para evitar fallos de mayus y minus
SELECT LEFT(nomequipo,1) FROM ciclista WHERE UPPER(LEFT(nombre,1))='R'; 

/* 6 - Listar el código de las etapas que su salida y llegada sea en la misma población */
SELECT numetapa FROM etapa WHERE llegada=salida;

/* 7 - Listar el código de las etapas que su salida y llegada no sean en la misma población y
       que conozcamos el dorsal del ciclista que ha ganado la etapa */
SELECT numetapa FROM etapa WHERE llegada<>salida AND dorsal IS NOT NULL;

/* 8 - Listar el nombre de los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400 */
SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura>2400;

/* 9 - Listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor 2400 */
SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura>2400;

/* 10 - Listar el número de ciclistas que hayan ganado alguna etapa */
SELECT COUNT(DISTINCT dorsal) FROM etapa;

-- esto en access hubiera sido 
  -- c1: ciclistas que han ganado etapas (sin repetidos)
  SELECT DISTINCT dorsal FROM etapa;

  SELECT COUNT(*) FROM (SELECT DISTINCT dorsal FROM etapa) AS c1; 

/* 11 - Listar el número de etapas que tengan puerto */
SELECT COUNT(DISTINCT numetapa) FROM puerto;

/* 12 - Listar el número de ciclistas que hayan ganado algún puerto */
SELECT COUNT(DISTINCT dorsal) FROM puerto;

/* 13 - Listar el código de la etapa con el número de puertos que tiene */
SELECT 
  numetapa,COUNT(*) puertosGanados 
FROM 
  puerto  
GROUP BY 
  numetapa;

/* 14 - Indicar la altura media de los puertos */
SELECT AVG(altura)alturaMedia FROM puerto;

/* 15 - Indicar el código de etapa cuya altura media de sus puertos está por encima de 1500 */
SELECT numetapa FROM puerto GROUP BY numetapa HAVING AVG(altura)>1500;

-- utilizando subconsulta
SELECT numetapa,AVG(altura) AS aMedia FROM puerto GROUP BY numetapa;
SELECT numetapa FROM (SELECT numetapa,AVG(altura) AS aMedia FROM puerto GROUP BY numetapa) c1 WHERE aMedia>1500;

/* 16 - Indicar el número de etapas que cumplen con al condición anterior */
SELECT COUNT(*)totalEtapas FROM (SELECT numetapa FROM puerto GROUP BY numetapa HAVING AVG(altura) >1500) c1;

/* 17 - Listar el dorsal del ciclista con el número de veces que ha llevado algún maillot */
SELECT dorsal,COUNT(*) maillotTotal FROM lleva GROUP BY dorsal;

/* 18 - Listar el dorsal del ciclista con el código de maillot y cuantas veces ese ciclista ha llevado ese maillot */
SELECT dorsal,código,COUNT(*)maillotTotal FROM lleva GROUP BY dorsal,código;

/* 19 - Listar el dorsal el código de etapa , el ciclista y el numero de maillots que ese ciclista a llevado en cada etapa */
SELECT dorsal,numetapa,COUNT(*)maillotTotal FROM lleva GROUP BY dorsal,numetapa;

-- si quisiermamos sacar el nombre 
SELECT 
  c1.*,c.nombre 
FROM 
  (SELECT dorsal,numetapa,COUNT(*)maillotTotal FROM lleva GROUP BY dorsal,numetapa) c1 
  INNER JOIN 
    ciclista c
  ON 
    c1.dorsal=c.dorsal;

-- hacer el join antes del group
  SELECT c.dorsal,l.numetapa,COUNT(*) AS numero FROM lleva l JOIN ciclista c ON l.dorsal=c.dorsal GROUP BY c.dorsal,l.numetapa;

