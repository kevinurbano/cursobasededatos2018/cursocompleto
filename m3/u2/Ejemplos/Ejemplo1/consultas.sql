﻿/* Consultas de seleccion - 1 */

/* 1 - Listar las edades de los ciclistas sin repetidos */

SELECT DISTINCT 
  edad 
FROM 
  ciclista;

/* 2 - Listar las edades de los ciclistas de Artiach */

SELECT DISTINCT
  edad -- proyeccion 3
FROM 
  ciclista -- tablas a utilizar 1
WHERE 
  nomequipo='Artiach'; -- seleccion 2

/* 3 - Listar las edades de los ciclistas de Artiach o Amore Vita */
SELECT DISTINCT 
  edad 
FROM 
  ciclista 
WHERE 
  nomequipo='Artiach' 
  OR 
  nomequipo='Amore Vita';

-- con IN
SELECT DISTINCT 
  edad 
FROM 
  ciclista 
WHERE 
  nomequipo IN ('Artiach','Amore Vita');

-- con UNION
SELECT DISTINCT 
  edad 
FROM 
  ciclista 
WHERE 
  nomequipo='Artiach' 
UNION
SELECT DISTINCT 
  edad 
FROM 
  ciclista 
WHERE 
  nomequipo='Amore Vita' ;


/* 4 - Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30 */
SELECT 
  dorsal 
FROM 
  ciclista 
WHERE 
  edad<25 
OR 
  edad>30;

/* 5 - Listar los dorsales de los ciclistas cuya edad este entre 28 y 32 y además que solo sean de Banesto */
SELECT dorsal 
FROM 
  ciclista 
WHERE 
  edad BETWEEN 28 AND 32 
  AND 
  nomequipo='Banesto';

-- Con JOIN
SELECT c2.dorsal 
FROM 
  (SELECT dorsal 
  FROM 
    ciclista 
  WHERE 
    edad BETWEEN 28 AND 32) AS C1
JOIN
  (SELECT dorsal 
  FROM 
    ciclista 
  WHERE 
    nomequipo='Banesto') AS C2
ON
  C1.dorsal=C2.dorsal;
  

/* 6 - Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8 */
SELECT DISTINCT 
  nombre 
FROM 
  ciclista 
WHERE 
  CHAR_LENGTH(nombre)>8;

/* 7 - Lístame el nombre y el dorsal de todos los ciclistas mostrando  un campo nuevo denominado nombre mayusculas que debe mostrar el nombre en mayúsculas */
  SELECT nombre,dorsal, UPPER(nombre) 'nombre Mayusculas' FROM ciclista;

/* 8 - Listar todos los ciclistas que han llevado el maillot MGE (amarillo) en alguna etapa */
SELECT DISTINCT dorsal FROM lleva WHERE código='MGE';

/* 9 - Listar el nombre de los puertos cuya altura sea mayor que 1500 */
SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500;

/* 10 - Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000 */
SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000;

-- C1 UNION C2
SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 
UNION
SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8;


/* 11 - Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000 */
SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000;

-- INTERSECT (NO EXISTE EN MYSQL SE UTILIZA JOIN) se utiliza intersect a nivel preguntas por separado
SELECT c1.dorsal 
  FROM
    (SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 ) c1
  JOIN
    (SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8) c2
  ON c1.dorsal=c2.dorsal;
 