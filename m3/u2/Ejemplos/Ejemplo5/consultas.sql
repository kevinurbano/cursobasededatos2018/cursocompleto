﻿/* Consultas de Combinación Externa - 5 */

/* 1 - Nombre y edad de los ciclistas que NO han ganado etapas */
SELECT DISTINCT nombre,edad FROM ciclista c LEFT JOIN etapa e ON c.dorsal=e.dorsal WHERE e.dorsal IS NULL;

-- optimizada
-- C1 ciclistas ganadores 
CREATE OR REPLACE VIEW e1c1 AS 
  SELECT DISTINCT dorsal FROM etapa;

-- C2 Todos los ciclistas
CREATE OR REPLACE VIEW e1c2 AS
  SELECT dorsal FROM ciclista;

-- C3: C1 - C2
CREATE OR REPLACE VIEW e1c3 AS
  SELECT c2.dorsal FROM e1c2 c2 LEFT JOIN e1c1 c1 USING(dorsal)
    WHERE c1.dorsal IS NULL;
 
-- C3 Ciclistas que no han ganado etapas con WHERE
CREATE OR REPLACE VIEW e1c3a AS
  SELECT dorsal FROM e1c2 
    WHERE dorsal NOT IN (SELECT dorsal FROM e1c1);

-- Consulta completa
CREATE OR REPLACE VIEW e1 AS
SELECT nombre,edad FROM e1c3 JOIN ciclista USING(dorsal);
 
SELECT * FROM e1;

/* 2 - Nombre y edad de los ciclistas que NO han ganado puertos */

SELECT DISTINCT nombre,edad 
  FROM ciclista c 
  LEFT JOIN puerto p ON c.dorsal=p.dorsal 
  WHERE p.dorsal IS NULL;

-- C1 - Ciclistas que han ganado puerto
CREATE OR REPLACE VIEW e2c1 AS
  SELECT DISTINCT dorsal FROM puerto;

-- consulta completa
CREATE OR REPLACE VIEW e2 AS
SELECT c.nombre,c.edad FROM ciclista c LEFT JOIN e2c1 c1 USING(dorsal)
  WHERE c1.dorsal IS NULL;


/* 3 - Listar el director de los equipos que tengan ciclistas NO que hayan ganado NINGUNA etapa */
SELECT DISTINCT director 
FROM equipo e 
JOIN ciclista c ON e.nomequipo=c.nomequipo
LEFT JOIN etapa et ON c.dorsal = et.dorsal WHERE et.dorsal IS null;

CREATE OR REPLACE VIEW e3 AS
SELECT DISTINCT e.director
  FROM e1c3 c3
  JOIN ciclista c USING(dorsal)
  JOIN equipo e USING(nomequipo);

SELECT * FROM e3;

-- C1
SELECT DISTINCT c.nomequipo FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal WHERE e.dorsal IS NULL; 

-- equipo JOIN C1
SELECT 
  director 
  FROM 
    equipo e 
  JOIN (
    SELECT DISTINCT 
      c.nomequipo 
    FROM 
      ciclista c 
    LEFT JOIN 
      etapa e 
    ON 
      c.dorsal = e.dorsal 
    WHERE 
      e.dorsal IS NULL
  ) c1
ON e.nomequipo=c1.nomequipo;

/* 4 - Dorsal y nombre de los ciclistas que NO hayan llevado algun maillot */
SELECT c.dorsal,c.nombre FROM ciclista c LEFT JOIN lleva l ON c.dorsal = l.dorsal WHERE l.dorsal IS NULL;

CREATE OR REPLACE VIEW e4c1 AS
  SELECT DISTINCT dorsal FROM lleva;

-- ciclistas que no han llevado maillot
CREATE OR REPLACE VIEW e4c3 AS
  SELECT c2.dorsal FROM e1c2 c2 LEFT JOIN e4c1 c1 USING(dorsal) WHERE c1.dorsal IS NULL;

-- Consulta Completa
CREATE OR REPLACE VIEW e4 AS
SELECT c.dorsal,c.nombre FROM e4c3 c3 JOIN ciclista c USING(dorsal);

SELECT * FROM e4;

/* 5 - Dorsal y nombre de los ciclistas que NO hayan llevado maillot amarillo NUNCA */

-- Codigo del maillot amarillo
CREATE OR REPLACE VIEW e5c1 AS 
SELECT código FROM maillot WHERE color='amarillo';

-- dorsal de los ciclistas que han'llevado maillot amarillo
CREATE OR REPLACE VIEW e5c2 AS 
SELECT l.dorsal FROM lleva l JOIN e5c1 c1 USING(código);


-- Ciclistas que no han llevado maillot amarillo
CREATE OR REPLACE VIEW e5 AS 
SELECT c.dorsal,c.nombre FROM ciclista c LEFT JOIN e5c2 USING(dorsal) WHERE e5c2.dorsal IS NULL;

SELECT * FROM e5;
  
/* 6 - Indicar el numetapa de las etapas que NO tengan puertos*/
SELECT e.numetapa FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL;

-- Etapas que tienen puerto
CREATE OR REPLACE VIEW e6c1 AS
  SELECT DISTINCT numetapa FROM puerto;

-- Todas las etapas
CREATE OR REPLACE VIEW e6c2 AS
  SELECT DISTINCT numetapa FROM etapa;

-- Consulta Completa sin C2
CREATE OR REPLACE VIEW e6 AS
  SELECT e.numetapa FROM etapa e LEFT JOIN e6c1 USING(numetapa) WHERE e6c1.numetapa IS NULL;

SELECT * FROM e6;

-- consulta completa con C2
CREATE OR REPLACE VIEW e6b AS
  SELECT c2.numetapa FROM e6c2 c2 LEFT JOIN e6c1 c1 USING(numetapa)
    WHERE c1.numetapa IS NULL;

SELECT * FROM e6b;

/* 7 - Indicar la distancia media de las etapas que NO tengan puertos  */
SELECT AVG(e.kms) FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL;

/* 8 - Listar el número de ciclistas que NO hayan ganado alguna etapa */
-- C1
SELECT c.dorsal FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal WHERE e.dorsal IS NULL;

SELECT COUNT(*) FROM (SELECT c.dorsal FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal WHERE e.dorsal IS NULL) c1;

/* 9 - Listar el dorsal de los ciclistas que hayan ganado alguna etapa que NO tenga puerto */

SELECT DISTINCT e.dorsal FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL;

/* 10 - Listar el dorsal de los ciclistas que hayan ganado unicamente etapas que no tenga puertos */
SELECT c1.dorsal 
  FROM   
     (SELECT DISTINCT e.dorsal FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL) c1
  LEFT JOIN
     (SELECT DISTINCT e.dorsal FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa)  c2
  ON c1.dorsal=c2.dorsal
  WHERE c2.dorsal IS null;
    
SELECT DISTINCT e.dorsal FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa;

SELECT DISTINCT e.dorsal FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL ;  


