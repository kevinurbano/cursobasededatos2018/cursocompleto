﻿-- Ejemplo

-- Ciclistas que han ganado etapas y que no han ganado puertos

-- C1 - dorsales de ciclistas que ganaron etapas
CREATE OR REPLACE VIEW ec1 AS 
  SELECT DISTINCT dorsal FROM etapa;
-- C2 - dorsales de ciclistas que ganaron puertos
CREATE OR REPLACE VIEW ec2 AS 
  SELECT DISTINCT dorsal FROM puerto;


/* Al restar los dorsales que han ganado etapas y los dorsaes que han ganado puertos 
   Obtengo los dorsales de ciclistas que gan ganado etapas pero no puertos */ 
SELECT ec1.dorsal FROM ec1 LEFT JOIN ec2 USING(dorsal) WHERE ec2.dorsal IS NULL;