﻿/* Practica 2 - Consultas */

/* 1 - Indicar el número de ciudades que hay en la tabla ciudades */
SELECT COUNT(*) TotalCiudades FROM ciudad;

/* 2 - Indicar el nombre de las ciudades que tengan una población por encima de la población media */
SELECT AVG(población) FROM ciudad;
SELECT nombre FROM ciudad WHERE población>(SELECT AVG(población) FROM ciudad);

/* 3 - Indicar el nombre de las ciudades que tengan una población por debajo de la población media */
SELECT nombre FROM ciudad WHERE población<(SELECT AVG(población) FROM ciudad);

/* 4 - Indicar el nombre de la ciudad con la población máxima */
SELECT nombre FROM ciudad WHERE población=(SELECT MAX(población) FROM ciudad);

/* 5 - Indicar el nombre de la ciudad con a población mínima */
SELECT nombre FROM ciudad WHERE población=(SELECT MIN(población) FROM ciudad);

/* 6 - Indicar el número de ciudades que tengan una población por encima de la población media */
SELECT COUNT(*) TotalCiudad FROM ciudad WHERE población>(SELECT AVG(población) FROM ciudad);

/* 7 - Indicarme el número de personas que viven en cada ciudad */
SELECT ciudad,COUNT(*) TotalPersonas FROM persona GROUP BY ciudad;

/* 8 - Utilizando la tabla trabaja indicarme cuantas personas trabajan en cada una de las compañias */
SELECT compañia,COUNT(*) TotalPersonas FROM trabaja GROUP BY compañia;

/* 9 - Indicarme la compañia que mas trabajadores tiene */
SELECT compañia,COUNT(*) TotalPersonas FROM trabaja GROUP BY compañia;

SELECT MAX(TotalPersonas) FROM (SELECT COUNT(*) TotalPersonas FROM trabaja GROUP BY compañia) C1;

SELECT 
  C1.compañia 
  FROM 
    (SELECT compañia,COUNT(*) TotalPersonas FROM trabaja GROUP BY compañia) C1
  WHERE 
    C1.TotalPersonas=(SELECT MAX(TotalPersonas) FROM (SELECT COUNT(*) TotalPersonas FROM trabaja 
  GROUP BY 
    compañia) C1);

/* 10 - Indicarme el salario media de cada una de las compañias */
SELECT compañia,AVG(salario) FROM trabaja GROUP BY compañia;

/* 11 - Listarme el nombre de las personas y la población de la ciudad donde vive */
SELECT p.nombre,población FROM persona p JOIN ciudad c ON p.ciudad = c.nombre;

/* 12 - Listar el nombre de las personas , la calle donde vive y la población de la ciudad donde vive */
SELECT p.nombre,calle,población FROM persona p JOIN ciudad c ON p.ciudad = c.nombre;

/* 13 - Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañia para la que trabaja */
SELECT persona,persona.ciudad,compañia.ciudad CompañiaCiudad
  FROM trabaja 
  JOIN persona ON trabaja.persona = persona.nombre
  JOIN compañia ON trabaja.compañia = compañia.nombre;

/* 14 - Realizar el algebra relacional y explicar la siguiente consulta */
SELECT persona.nombre 
  FROM persona, trabaja, compañia
  WHERE persona.nombre=trabaja.persona
  AND trabaja.compañia=compañia.nombre
  AND compañia.ciudad=persona.ciudad
  ORDER BY persona.nombre;

/* 15 - Listame el nombre de la persona y el nombre de su supervisor */
SELECT supervisor,persona FROM supervisa;

/* 16 - Listarme el nombre de la persona, 
        el nombre de su supervisor y las ciudades donde residen cada uno de ellos */
SELECT supervisor,p1.ciudad,persona,p2.ciudad
  FROM supervisa 
  JOIN persona p1 ON persona=p1.nombre
  JOIN persona p2 ON supervisor=p2.nombre
ORDER BY persona;

/* 17 - Indicarme el número de ciudades distintas que hay en la tabla compañia */
SELECT COUNT(DISTINCT ciudad) FROM compañia;

/* 18 - Indicarme el número de ciudades distintas que hay en la tabla personas */
SELECT COUNT(DISTINCT ciudad) FROM persona;

/* 19 - Indicarme el nombre de las personas que trabajan para FAGOR */
SELECT DISTINCT persona FROM trabaja
  WHERE compañia="FAGOR";

/* 20 - Indicarme el nombre de las personas que no trabajan para FAGOR */
SELECT DISTINCT persona FROM trabaja
  WHERE NOT compañia="FAGOR";

/* 21 - Indicarme el número de personas que trabajan para INDRA */
SELECT COUNT(t.persona) 
  FROM trabaja t
  WHERE t.compañia="INDRA";
  
/* 22 - Indicarme el nombre de las personas que trabajan para FAGOR o para INDRA */


/* 23 - Listar la población donde vive cada pesona, su salario, 
        su nombre y la compañia para la que trabaja. 
        Ordenlar la salida por nombre de la persona y por salario de forma descendente. */
SELECT c.población,t.salario,p.nombre FROM persona p
  JOIN trabaja t ON p.nombre=t.persona
  JOIN ciudad c ON c.nombre=p.ciudad
  ORDER BY p.nombre,t.salario DESC;