﻿/* creando las bases de datos */
DROP DATABASE IF EXISTS practica2;
CREATE DATABASE IF NOT EXISTS practica2;
USE practica2;

CREATE TABLE Ciudad (
  nombre Varchar(30) PRIMARY KEY,
  población integer
);

CREATE TABLE Persona (
  nombre Varchar(30) PRIMARY KEY,
  calle Varchar(30),
  ciudad Varchar(30),
  CONSTRAINT FKPersonaCiudad FOREIGN KEY (ciudad)
    REFERENCES Ciudad(nombre) ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE Compañia (
  nombre Varchar(30) PRIMARY KEY,
  ciudad Varchar(30),
  CONSTRAINT FKCompaniaCiudad FOREIGN KEY (ciudad)
    REFERENCES Ciudad(nombre) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE Trabaja (
  persona Varchar(30) PRIMARY KEY,
  compañia Varchar(30),
  salario integer,
  CONSTRAINT FKTrabajaPersona FOREIGN KEY (persona)
    REFERENCES Persona(nombre) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKTrabajaCompania FOREIGN KEY (compañia)
    REFERENCES Compañia(nombre) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE Supervisa (
  supervisor Varchar(30),
  persona Varchar(30),
  PRIMARY KEY (supervisor,persona),
  CONSTRAINT FKSupervisaSupervisor FOREIGN KEY (persona)
    REFERENCES Persona(nombre) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKSupervisaPersona FOREIGN KEY (persona)
    REFERENCES Persona(nombre) ON DELETE CASCADE ON UPDATE CASCADE
);

