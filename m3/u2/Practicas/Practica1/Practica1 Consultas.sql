﻿USE practica1; -- Selecciono la base de datos con la que voy a trabajar

/* Practica Número: 1 */

/* 
  Consulta 1 
*/

SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no 
FROM 
  emple e; 

/* 
  Consulta 2
*/

SELECT 
  d.dept_no,
  d.dnombre,
  d.loc   
FROM 
  depart d;

/*
  Consulta 3 
*/

SELECT DISTINCT e.apellido, e.oficio FROM emple e;

/* 
  Consulta 4
*/

SELECT d.dept_no, d.loc FROM depart d;

/* 
  Consulta 5
*/

SELECT 
       d.dept_no,
       d.dnombre,
       d.loc
FROM  
  depart d;

/*
  Consulta 6
*/

SELECT COUNT(*) numEmpleados FROM   emple e;

/*
  Consulta 7
*/

SELECT 
       emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no 
FROM 
  emple
ORDER BY 
  apellido ASC;

/*
  Consulta 8  
*/

SELECT 
       emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no 
FROM 
  emple
ORDER BY 
  apellido DESC;

/*
  Consulta 9
*/

SELECT COUNT(*) numDepartamentos FROM depart;

/*
  Consulta 10
*/

SELECT (SELECT COUNT(*) FROM emple) + (SELECT COUNT(*) FROM depart);

/*
  Consulta 11
*/

SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no
FROM 
  emple 
ORDER BY
  dept_no DESC;

/*
  Consulta 12
*/

SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no 
FROM 
  emple
ORDER BY
  dept_no DESC,
  oficio ASC;

/*
  Consulta 13
*/

SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no
FROM 
  emple
ORDER BY
  dept_no DESC,
  apellido ASC;

/*
  Consulta 14
*/

SELECT emp_no FROM emple WHERE salario>2000;

/*
  Consulta 15
*/

SELECT emp_no,apellido FROM emple WHERE salario<2000;

/*
  Consulta 16
*/

SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no 
FROM 
  emple 
WHERE 
  salario BETWEEN 1500 AND 2500;

/*
  Consulta 17
*/

SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no 
FROM 
  emple 
WHERE 
  oficio='ANALISTA';

/*
  Consulta 18
*/

SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no 
FROM 
  emple 
WHERE 
  oficio='ANALISTA' 
AND 
  salario>2000;

/*
  Consulta 19
*/

SELECT apellido,oficio FROM emple WHERE dept_no=20;

/*
  Consulta 20
*/

SELECT COUNT(*) FROM emple WHERE oficio='VENDEDOR';

/*
  Consulta 21
*/

SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no
FROM 
  emple 
WHERE 
  apellido LIKE "m%" 
OR 
  apellido LIKE "n%"
ORDER BY 
  apellido ASC;

/*
  Consulta 22
*/

SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no  
FROM 
  emple 
WHERE 
  oficio='VENDEDOR' 
ORDER BY 
  apellido ASC;

/*
  Consulta 23 -?
*/

SELECT apellido FROM emple WHERE salario=(SELECT MAX(salario) FROM emple);

/*
  Consulta 24
*/
 
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no 
FROM 
  emple 
WHERE 
  dept_no=10 
AND 
  oficio='ANALISTA' 
ORDER BY 
  apellido DESC,
  oficio DESC;

/*
  Consulta 25 -?
*/

SELECT DISTINCT MONTH(fecha_alt) Meses FROM emple ORDER BY Meses;

/*
  Consulta 26 -?
*/

SELECT DISTINCT YEAR(fecha_alt) Anios FROM emple ORDER BY Anios;

/*
  Consulta 27 -?
*/

SELECT DAY(fecha_alt) Dias, MONTH(fecha_alt) Meses FROM emple GROUP BY Dias,Meses ORDER BY Meses;

/*
  Consulta 28 
*/

SELECT DISTINCT apellido FROM emple WHERE salario>2000 OR dept_no=20;

/*
  Consulta 29 
*/

SELECT emple.apellido,
       depart.dnombre 
FROM 
  emple INNER JOIN depart ON emple.dept_no =depart.dept_no;

/*
  Consulta 30 
*/

SELECT emple.apellido,
       emple.oficio,
       depart.dnombre 
FROM 
  emple INNER JOIN depart ON emple.dept_no =depart.dept_no
ORDER BY
  apellido DESC;

/*
  Consulta 31 
*/

SELECT dept_no,COUNT(*) NUMERO_DE_EMPLEADOS FROM emple  GROUP BY dept_no;

/*
  Consulta 32 
*/

SELECT 
  depart.dnombre,
  COUNT(*) NUMERO_DE_EMPLEADOS 
FROM 
  emple INNER JOIN depart ON emple.dept_no=depart.dept_no  
GROUP BY 
  depart.dnombre;

/*
  Consulta 33 -?
*/

SELECT apellido
FROM emple INNER JOIN depart ON emple.dept_no=depart.dept_no  
ORDER BY oficio, dnombre;

/*
  Consulta 34
*/

SELECT apellido FROM emple WHERE apellido LIKE 'A%';

/*
  Consulta 35
*/

SELECT apellido FROM emple WHERE apellido LIKE 'A%' OR apellido LIKE 'M%';

/*
  Consulta 36
*/

SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no 
FROM 
  emple
WHERE 
  apellido NOT LIKE '%Z';

/*
  Consulta 37
*/

SELECT apellido,
       oficio 
FROM 
  emple 
WHERE 
  apellido LIKE 'A%' 
AND 
  oficio LIKE '%E%' 
ORDER BY 
  oficio DESC,
  salario DESC;