﻿/* LDD - Lenguaje de definicion de datos */
-- Gestion de tablas  

-- Eliminar base de datos si existe
DROP DATABASE IF EXISTS hoja7unidad1modulo3;

-- Crear base de datos si no existe
CREATE DATABASE IF NOT EXISTS hoja7unidad1modulo3;

-- Utilizar base de datos
USE hoja7unidad1modulo3;

-- Eliminar la tabla si existe 
DROP TABLE IF EXISTS empleado;

-- Crear la tabla si no existe
-- Hoja de Ejercicios 7
CREATE TABLE IF NOT EXISTS empleado(
  dni char(9),
  PRIMARY KEY(dni)
  );

INSERT INTO empleado VALUES 
  ('dni1'),
  ('dni2');

DROP TABLE IF EXISTS departamento; 
CREATE TABLE IF NOT EXISTS departamento(
  codigo varchar(15),
  PRIMARY KEY(codigo)
  );

INSERT INTO departamento VALUES
  ('departamento1'),
  ('departamento2');

-- Eliminar la tabla 
DROP TABLE IF EXISTS pertenece;

-- Crear la tabla 
CREATE TABLE IF NOT EXISTS pertenece (
  depertamento varchar(15),
  empleado char(9),
  PRIMARY KEY (depertamento,empleado),
  CONSTRAINT uniqueEmpleado UNIQUE KEY (empleado),
  CONSTRAINT FKPerteneceEmpleado FOREIGN KEY(empleado) 
    REFERENCES empleado(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKPerteneceDepartamento FOREIGN KEY(depertamento) 
    REFERENCES departamento(codigo) ON DELETE CASCADE ON UPDATE CASCADE
  );

-- Eliminar la tabla 
DROP TABLE IF EXISTS proyecto;

-- Crear tabla
CREATE TABLE IF NOT EXISTS proyecto (
  codigoProyecto varchar(15),
  PRIMARY KEY (codigoProyecto)
  );

-- Eliminar tabla
DROP TABLE IF EXISTS trabaja;

-- Crear tabla
CREATE TABLE IF NOT EXISTS trabaja(
  empleado char(9),
  proyecto varchar(15),
  fecha date,
  PRIMARY KEY (empleado,proyecto),
  CONSTRAINT FKTrabajaEmpleado FOREIGN KEY(empleado)
    REFERENCES empleado(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKTrabajaProyecto FOREIGN KEY(proyecto)
    REFERENCES proyecto(codigoProyecto) ON DELETE CASCADE ON UPDATE CASCADE
  );
