﻿DROP DATABASE IF EXISTS hoja7unidad1modulo3ejercicio6;

CREATE DATABASE IF NOT EXISTS hoja7unidad1modulo3ejercicio6;

USE hoja7unidad1modulo3ejercicio6;

DROP TABLE IF EXISTS opcion;
CREATE TABLE IF NOT EXISTS opcion(
  nombre varchar(10),
  descripcion text,
  PRIMARY KEY (nombre)
  );

DROP TABLE IF EXISTS modelo;
CREATE TABLE IF NOT EXISTS modelo(
  marca varchar(20),
  cilindrada int,
  modelo varchar(20),
  precio float,
  PRIMARY KEY (marca,cilindrada,modelo),
  CHECK(precio>0)
  );

DROP TABLE IF EXISTS tiene;
CREATE TABLE IF NOT EXISTS tiene(
  opcion varchar(10),
  marca varchar(20),
  cilindrada int,
  modelo varchar(20),
  precio float,
  PRIMARY KEY (opcion,marca,cilindrada,modelo),
  CONSTRAINT FKTieneModelo FOREIGN KEY (marca,cilindrada,modelo)
    REFERENCES modelo(marca,cilindrada,modelo) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKTieneOpcion FOREIGN KEY (opcion)
    REFERENCES opcion(nombre) ON DELETE CASCADE on UPDATE CASCADE
  );

DROP TABLE IF EXISTS vendedor;
CREATE TABLE IF NOT EXISTS vendedor(
  nombre varchar(10),
  descuento varchar(20),
  PRIMARY KEY (nombre)
  );

DROP TABLE IF EXISTS cliente;
CREATE TABLE IF NOT EXISTS cliente(
  dni varchar(10),
  nombre varchar(20),
  direccion varchar(10),
  telefono varchar(10),
  PRIMARY KEY (dni)
  );

DROP TABLE IF EXISTS vehiculos;
CREATE TABLE IF NOT EXISTS vehiculos(
  matricula varchar(10),
  precio int(10),
  marca varchar(10),
  modelo varchar(10),
  PRIMARY KEY (matricula)
  );

DROP TABLE IF EXISTS compra;
CREATE TABLE IF NOT EXISTS compra(
  marca varchar(20),
  cilindrada int,
  modelo varchar(20),
  cliente varchar(10),
  vendedor varchar(10),
  opcion  varchar(10),
  PRIMARY KEY(marca,cilindrada,modelo,cliente,vendedor,opcion),
  CONSTRAINT FKCompraModelo FOREIGN KEY (marca,cilindrada,modelo)
   REFERENCES modelo(marca,cilindrada,modelo) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKCompraCliente FOREIGN KEY (cliente)
   REFERENCES cliente(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKCompraVendedor FOREIGN KEY (vendedor)
   REFERENCES vendedor(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKCompraOpcion FOREIGN KEY (opcion)
   REFERENCES opcion(nombre) ON DELETE CASCADE ON UPDATE CASCADE
  );

DROP TABLE IF EXISTS cede;
CREATE TABLE IF NOT EXISTS cede(
  cliente varchar(10),
  vehiculo varchar(10),
  fecha date,
  PRIMARY KEY (cliente,vehiculo),
  CONSTRAINT cedeUnica1 UNIQUE KEY(vehiculo),
  CONSTRAINT FKCedeCliente FOREIGN KEY (cliente)
   REFERENCES cliente(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKCedeVehiculo FOREIGN KEY (vehiculo)
   REFERENCES vehiculos(dni) ON DELETE CASCADE ON UPDATE CASCADE
  );



