﻿-- Hoja7 Modulo 3 Unidad 1 - Ejercicio 3
DROP DATABASE IF EXISTS hoja7unidad1modulo3ejercicio3;

CREATE DATABASE IF NOT EXISTS hoja7modulo3unidad1ejercicio3;

USE hoja7modulo3unidad1ejercicio3;

DROP TABLE IF EXISTS turista;
CREATE TABLE IF NOT EXISTS turista(
  t int(5),
  nombre varchar(10),
  apellidos varchar(20),
  direccion varchar (10),
  telefono int(9),
  PRIMARY KEY (t)
  );

DROP TABLE IF EXISTS vuelo;
CREATE TABLE IF NOT EXISTS vuelo(
  n int(5),
  fecha date,
  hora time,
  origen varchar(15),
  destino varchar(15),
  numTurista int(3),
  numTotal int(3),
  PRIMARY KEY (n)
  );

DROP TABLE IF EXISTS agencia;
CREATE TABLE IF NOT EXISTS agencia(
  s int(5),
  direccion varchar(15),
  telefono int(9),
  PRIMARY KEY (s)
  );

DROP TABLE IF EXISTS hotel;
CREATE TABLE IF NOT EXISTS hotel(
  h int(5),
  nombre varchar(10),
  direccion varchar(15),
  ciudad varchar(10),
  plazas int(5),
  telefono int(9),  
  PRIMARY KEY (h)
  );

DROP TABLE IF EXISTS toma;
CREATE TABLE IF NOT EXISTS toma(
  turista int(5),
  vuelo int(5),
  clase varchar(10),
  PRIMARY KEY (turista,vuelo),
  CONSTRAINT FKTomaTurista FOREIGN KEY (turista)
    REFERENCES turista(t) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKTomaVuelo FOREIGN KEY (vuelo)
    REFERENCES vuelo(n) ON DELETE CASCADE ON UPDATE CASCADE
  );

DROP TABLE IF EXISTS contrata;
CREATE TABLE IF NOT EXISTS contrata(
  turista int(5),
  agencia int(5),
  PRIMARY KEY(turista,agencia),
  CONSTRAINT FKContrataTurista FOREIGN KEY (turista)
    REFERENCES turista(t) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKContrataAgencia FOREIGN KEY (agencia)
    REFERENCES agencia(s) ON DELETE CASCADE ON UPDATE CASCADE
  );

DROP TABLE IF EXISTS reserva;
CREATE TABLE IF NOT EXISTS reserva(
  turista int(5),
  hotel int(5),
  fecha_e date,
  fecha_s timestamp, 
  pension int(5),
  PRIMARY KEY (turista, hotel),
  CONSTRAINT FKReservaTurista FOREIGN KEY (turista)
    REFERENCES turista(t) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKReservaHotel FOREIGN KEY (hotel)
    REFERENCES hotel(h) ON DELETE CASCADE ON UPDATE CASCADE
  );









